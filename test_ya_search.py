from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
import allure
from allure_commons import types


@allure.title('Результат поиска в ЯНДЕКСЕ больше 10')
@allure.severity(types.Severity.BLOCKER)
def test_ya_search():
    driver = WebDriver(executable_path='C://Selenium//chromedriver.exe')
    with allure.step('Открываем страницу поиска'):
        driver.get('https://ya.ru')

    with allure.step('Ищем market.yandex.ru'):
        search_input = driver.find_element_by_xpath('//input[@id="text"]')
        search_button = driver.find_element_by_xpath('// button[@ type="submit"]')
        search_input.send_keys('market.yandex.ru')
        search_button.click()

    def check_results_count(driver):
        inner_search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
        return len(inner_search_results) >= 10
    with allure.step('Ожидаем что значение количества результатов теста больше 10'):
        WebDriverWait(driver, 5, 0.5).until(check_results_count, 'КОЛЛИЧЕСТВО РЕЗУЛЬТАТОВ ПОИСКА МЕНЬШЕ 10')

    with allure.step('Переходим по ссылке первого результата'):
        search_results = driver.find_elements_by_xpath('//li[@class="serp-item"]')
        link = search_results[0].find_element_by_xpath('.//h2/a')
        link.click()


    driver.switch_to.window(driver.window_handles[1])
    with allure.step('Сверяем title старницы'):
        assert driver.title == 'Яндекс.Маркет — выбор и покупка товаров из проверенных интернет-магазинов'

    #pythcheck_results_count(driver)

