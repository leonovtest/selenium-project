selenium==3.141.0
allure-pytest==2.8.6
pytest==5.3.1
faker # Библиотека для генерации тестовых данных
flake8 # Библиотека для проверки code-style
flake8-import-order # Плагин для библиотеки выше
requests # Библиотека для отправки запросов
tox # А эта библиотека поможет нам быстро проводить валидацию наших тестов